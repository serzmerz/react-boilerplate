import { call, put } from 'redux-saga/effects';

/**
 * Function helper ensure
 * @function name ensure;
 * @param api {function} method Api
 * @param action {object} contain string params success ans failure
 * @param serializer {function} serialize function
 * @param putGen
 * @param name {string}
 * name of in arguments of returned generator that need to include to serialize function
 * */
export default function ensure(
  {
    api, action, serializer = d => d, putGen = put,
  }, name = 'payload',
) {
  // eslint-disable-next-line func-names
  return function* (args = {}) {
    const defaultArgs = Object.assign({ params: {} }, args);
    try {
      const { data } = yield call(api, args);
      yield putGen({
        type: action.success,
        payload: serializer(data, args[name]),
        meta: { ...defaultArgs.params, ...(data.meta || {}) },
      });
    } catch (err) {
      yield putGen({
        type: action.failure,
        err,
      });
    }
  };
}
